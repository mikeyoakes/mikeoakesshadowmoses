﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovementManager {
    void MovePlayer(string direction, GameObject player);
}

public class MovementManager:IMovementManager
{
    public readonly float moveSpeed = 1;

    public void MovePlayer(string direction, GameObject player)
    {
        Debug.Log("Name is " + player.name);

        // Rotate left
        if (direction == "LEFT")
        {
            player.transform.Rotate(0, -1, 0);
        }
        // Rotate right
        else if (direction == "RIGHT")
        {
            player.transform.Rotate(0, 1, 0);
        }
        // Strafe left
        else if (direction == "UP")
        {
            player.transform.position += player.transform.forward * moveSpeed;
        }
        // Move backward
        else if (direction == "DOWN")
        {
            player.transform.position -= player.transform.forward * moveSpeed;
        }
        Debug.Log("Moved Player");




    }

}
