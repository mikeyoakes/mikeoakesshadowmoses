﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public interface IInputManager
{
    void CheckInput(GameObject gameObject);
}

public class InputManager : IInputManager
{
    private IMovementManager moveMgr;

    // This will automatically inject the movement manager into player at runtime.
    // This is completed through the MonoInstaller - it does not have to be called
    // explicitly.
    [Inject]
    public void SetupInject(IMovementManager initMoveMgr)
    {
        this.moveMgr = initMoveMgr;
    }



    public void CheckInput(GameObject player)
    {
        // Rotate left
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveMgr.MovePlayer("LEFT", player);
        }
        // Rotate right
        if (Input.GetKey(KeyCode.RightArrow))
        {
            moveMgr.MovePlayer("RIGHT", player);
        }
        // Move forward
        //if (Input.GetKey(KeyCode.W))
        if (Input.GetKey(KeyCode.UpArrow))
        {
            moveMgr.MovePlayer("UP", player);
        }
        // Move backward
        if (Input.GetKey(KeyCode.DownArrow))
        {
            moveMgr.MovePlayer("DOWN", player);
        }


        /*
        Debug.Log("CHECKINPUT: Moved Player");
        Debug.Log("CHECKINPUT: Name is " + player.name);
        */
    }

}

