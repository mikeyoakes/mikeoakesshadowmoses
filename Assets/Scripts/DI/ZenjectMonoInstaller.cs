using UnityEngine;
using Zenject;

public class ZenjectMonoInstaller : MonoInstaller
{

    public override void InstallBindings()
    {
        Container.Bind<IMovementManager>().To<MovementManager>().AsSingle();
        Container.Bind<IInputManager>().To<InputManager>().AsSingle();
    }
}