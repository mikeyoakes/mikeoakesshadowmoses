﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Player : MonoBehaviour {

    private IInputManager inputMgr;

    // This will automatically inject the input manager into player at runtime.
    // This is completed through the MonoInstaller - it does not have to be called
    // explicitly.
    [Inject]
    public void SetupInject(IInputManager initInputMgr)
    {
        this.inputMgr = initInputMgr;
    }

	
	// Update is called once per frame
	void Update () {
        inputMgr.CheckInput(this.gameObject);
    }
}
