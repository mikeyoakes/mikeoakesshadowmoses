# MikeOakesShadowMoses

Zenject Basic Character Controller Project.

Unity 2018.2.21f1

The project consists of one Scene:  MainZenjectDemo


The scene has the following gameobjets:

SceneContext - The Zennject Scene Context.

Chicken - The Chicken(player) can be moved around using the arrow keys, with a RigidBody added to stop it moving through the wall, floor.

Floor - The main environment consisting of the floor and a surrounding wall, both with a Rigidbody  added to stop it moving through the wall, floor. 



The scene has the following scripts:

ZenjectMonoInstaller - A Zenject MonoInstaller containing the bindings for IInputManager/InputManager and IMovementManager/MovementManager both as Singletons.

Player - a class attached as a component to the Chicken gameobject, derived from Monobehaviour, and having an IInputManager injected. 
 Its Update method calls the IInputManager.CheckInput method passing the reference to the Chicken gameobject. 

IInputManager - an interface containing one method, CheckInput that accepts the target gameobject to move as a parameter.

InputManager - a class implementing the IInputManager interface, and having an IMovementManager injected.  
  Methods:   
			CheckInput[ gameobject player]
				Accepts the player gameobject that will be subject to any operations as a parameter and looks for the different inputs from the arrow keys.  
				On receiving and input from one of the arrow keys it calls the MovementManager.MovePlayer method to move the player, passing parameters to 
				indicate the movement direction and target gameobject to move.


IMovementManager - an interface containing one method.  
  Methods:   
			MovePlayer[ string direction, gameobject player]
			Accepts the direction to move the target gameobject in, and the target gameobject to  be moved. If LEFT or RIGHT directions are passed then it
			rotates the player fameobject in that direction, if UP or DOWN are passed then it moves the player gameobject in that direction.